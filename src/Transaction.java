
public interface Transaction {
	public int cashDeposit(int amount);
	public int cashWithdraw(int amount);
	public int checkBalance();
	public int updateBalance(int bal);
	public int updatePassword(String pwd);
	public int updateEmail(String email);
	public int signUp();
	
}
